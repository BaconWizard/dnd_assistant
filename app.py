from flask import Flask, render_template, request

from flask import *

import os

path = os.path.dirname(os.path.realpath(__file__))

app = Flask(__name__)

print(path)

@app.route('/', methods=['GET', 'POST'])
def home():
    
    return render_template("home_not_logged.html")


@app.route('/login')
def login():
    return render_template("Login.html")
    



if __name__ == '__main__':
    app.run()
